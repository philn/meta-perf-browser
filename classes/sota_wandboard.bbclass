# SOTA support
OSTREE_BOOTLOADER = "u-boot"

WKS_FILES_wandboard = "imx-uboot-spl-ostree.wks"
WKS_FILES_wandboard-mesa = "imx-uboot-spl-ostree.wks"

IMAGE_INSTALL_append_sota = " u-boot-otascript"

INITRAMFS_FSTYPES = "cpio.gz.u-boot"
 
PREFERRED_PROVIDER_virtual/bootloader_sota ?= "u-boot"

INITRAMFS_IMAGE = "initramfs-wandboard-ostree-image"
 
UBOOT_EXTLINUX = "0"

# OSTree puts its own boot.scr to wandboard-bootfiles
IMAGE_BOOT_FILES = " \
    ${@make_dtb_boot_files(d)} \
    wandboard-bootfiles/* \
"

SOTA_CLIENT_FEATURES_append = " ubootenv"
