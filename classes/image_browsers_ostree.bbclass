DESCRIPTION += "(OSTree)"

REQUIRED_DISTRO_FEATURES += "sota usrmerge"

SOTA_CLIENT = ""
SOTA_CLIENT_PROV = ""
SOTA_DEPLOY_CREDENTIALS = "0"
SOTA_HARDWARE_ID ??= "${MACHINE}"

IMAGE_INSTALL_append_sota = " ostree os-release ostree-provisioner watchdog python3-ansible"
IMAGE_FSTYPES_remove = "garagesign"
IMAGE_FSTYPES_remove = "garagecheck"
IMAGE_FSTYPES_remove = "ostreepush"

# Please redefine OSTREE_REPO in order to have a persistent OSTree repo
export OSTREE_REPO ?= "${DEPLOY_DIR_IMAGE}/ostree_repo"
export OSTREE_BRANCHNAME ?= "${SOTA_HARDWARE_ID}"
export OSTREE_OSNAME = "browsers"
export OSTREE_BOOTLOADER = 'u-boot'
export OSTREE_BOOT_PARTITION = "/boot"

inherit ${@bb.utils.contains('MACHINE_ARCH','wandboard_mesa','sota_wandboard','', d)}
inherit ${@bb.utils.contains('MACHINE_ARCH','wandboard_vivante','sota_wandboard','', d)}
