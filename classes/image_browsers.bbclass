DESCRIPTION += " with browsers support"
LICENSE = "MIT"

IMAGE_FEATURES += "splash package-management hwcodecs ssh-server-openssh"

inherit core-image distro_features_check

IMAGE_LINGUAS = "en-us es-es"

# IMAGE_OVERHEAD_FACTOR = "1.5"
# IMAGE_ROOTFS_EXTRA_SPACE = "2097152"

EXTRA_IMAGE_FEATURES .= " debug-tweaks"
## EXTRA_IMAGE_FEATURES .= " debug-tweaks dbg-pkgs tools-debug tools-profile"
# By default, the Yocto build system strips symbols from the binaries it
# packages, which makes it difficult to use some of the tools.
# 
# You can prevent that by setting the INHIBIT_PACKAGE_STRIP variable to "1" in
# your local.conf when you build the image:
INHIBIT_PACKAGE_STRIP = "1"

IMAGE_INSTALL_append = " libtasn1 htop nano strace bridge-utils ntp curl dhcp-client lzo"


SDK_EXTRA_TOOLS += "nativesdk-cmake nativesdk-ninja \
    nativesdk-perl-module-findbin \
    nativesdk-perl-misc \
    "
TOOLCHAIN_HOST_TASK_append = "${SDK_EXTRA_TOOLS}"
TOOLCHAIN_TARGET_TASK_remove = "target-sdk-provides-dummy"

PACKAGECONFIG_append_pn-php = " apache2"
