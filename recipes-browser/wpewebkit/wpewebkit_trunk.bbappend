FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

S = "${WORKDIR}/git"
SRC_URI = "git://github.com/WebKit/webkit.git;protocol=git;branch=master \
           file://keep_cplusplus_201703_check_in_StdLibExtras.patch \
"

PACKAGECONFIG[accessibility] = "-DENABLE_ACCESSIBILITY=ON,-DENABLE_ACCESSIBILITY=OFF,atk"

PV = "nightly-${SRCPV}"
SRCREV = "${AUTOREV}"
