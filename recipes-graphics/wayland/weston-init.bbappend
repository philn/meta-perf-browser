FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://weston.default"


do_install_append () {
    install -d ${D}/${sysconfdir}/default/
    install -m 644 ${WORKDIR}/weston.default ${D}/${sysconfdir}/default/weston
}
