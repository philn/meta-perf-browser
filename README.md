# Getting the BSP

* Install `repo`:  `apt-get install repo`
* Install the required Yocto systemd dependencies in your system: https://www.yoctoproject.org/docs/1.8/yocto-project-qs/yocto-project-qs.html#packages

  * In Debian/Ubuntu: https://www.yoctoproject.org/docs/1.8/yocto-project-qs/yocto-project-qs.html#ubuntu
  * In Fedora: https://www.yoctoproject.org/docs/1.8/yocto-project-qs/yocto-project-qs.html#fedora

* Create your workdir:

  ```bash
  CI_COMMIT_REF_NAME="master"

  mkdir -p ~/perf-browser-bsp
  cd ~/perf-browser-bsp
  repo init -u https://gitlab.com/saavedra.pablo/meta-perf-browser.git -m manifest.xml -b $CI_COMMIT_REF_NAME
  repo sync --force-sync
  ```

# Activating your work environment

Several environments can be defined your build environment. For example:

* RPI2:

  ```bash
  $ source setup-environment rpi2-mesa-wpe-nightly raspberrypi2-mesa browsers raspberrypi mesa-wpe-nightly --update-config
  ``` 

* RPI3 using the propietary graphics stack (vc4graphics):

  ```bash
  $ source setup-environment rpi3-userland-wpe-nightly raspberrypi3-userland browsers raspberrypi mesa-wpe-nightly --update-config
  ``` 
* Wandboard using the propietary graphics stack (vivante) and WPE 2.24:

  ```bash
  $ source setup-environment  wandboard-vivante-wpe-2.24  wandboard-vivante  browsers-lsb  wandboard vivante-wpe-2_24 --update-config
  ```

See more options running the command with no arguments:

```bash
$ source setup-environment 
Usage: setup-environment targetname
       setup-environment targetname machine distro bblayers presets --update-config

Targets:

<<new>>
mynewone
rpi2-mesa-wpe-nightly
rpi3
rpi3-vc4graphics-wpe-2.22
wandboard
wandboard-mesa-wpe-2.20
wandboard-mesa-wpe-alternative
wandboard-mesa-wpe-nightly
wandboard-mesa-wpe-trunk
wandboard-mesa-wpe-trunk.old
wandboard-vivante-wpe-2.20
wandboard-vivante-wpe-2.24
wandboard-vivante-wpe-nightly
wandboard-vivante-wpe-trunk

Machines:

raspberrypi2-mesa
raspberrypi2-userland
raspberrypi3-mesa
raspberrypi3-userland
wandboard-mesa
wandboard-vivante

Distros:

browsers
browsers-lsb
browsers-sota

BBlayers:

raspberrypi
wandboard

Presets:

mesa-wpe-2_20
mesa-wpe-2_22
mesa-wpe-2_24
mesa-wpe-alternative
mesa-wpe-experimental
mesa-wpe-nightly
mesa-wpe-qt
mesa-wpe-trunk
raspberrypi3
userland-wpe-2_22
vivante-wpe-2_20
vivante-wpe-2_22
vivante-wpe-nightly
vivante-wpe-trunk
```

# Building the image

Once activated your working environment, you can build your target image:

```bash
 $ bitbake-layers show-recipes  "*image*" | grep -B 1 meta-perf-browser
core-image-weston-wpe:
  meta-perf-browser    1.0
core-image-wpe:
  meta-perf-browser    1.0
core-image-wpe-base:
  meta-perf-browser    1.0
```

```bash
rm -rf tmp
bitbake core-image-weston-wpe
```

The image will be available in `tmp/deploy/images/<<machine>>/`


# SDK Toolchain


## Preparing the SDK Toolchain

The following steps are for a Wandboard+mesa but you can reuse it with small changes for any other machine variant:

```bash
source setup-environment wandboard-mesa-wpe-base-trunk wandboard-mesa browsers wandboard mesa-wpe-trunk --update-config
rm -rf tmp
bitbake core-image-wpe-base -c populate_sdk
# The resulting image:
ls ./tmp/deploy/sdk/browsers-glibc-x86_64-core-image-wpe-base-armv7at2hf-neon-toolchain-2.6.2.sh
```

## Installing the SDK Toolchain

```bash
./tmp/deploy/sdk/browsers-glibc-x86_64-core-image-wpe-base-armv7at2hf-neon-toolchain-2.6.2.sh -d ~/toolchain_env/wandboard-mesa-wpe-trunk -y
```

## Activating the SDK Toolchain

```bash
. ~/toolchain_env/wandboard-mesa-wpe-trunk/environment-setup-armv7at2hf-neon-poky-linux-gnueabi
```

```bash
$ env | grep OE
OECORE_ACLOCAL_OPTS=-I
/home/igalia/psaavedra/toolchain_env/wandboard-mesa-wpe-trunk/sysroots/x86_64-pokysdk-linux/usr/share/aclocal
OECORE_DISTRO_VERSION=1.0
OECORE_SDK_VERSION=2.6.1
OECORE_NATIVE_SYSROOT=/home/igalia/psaavedra/toolchain_env/wandboard-mesa-wpe-trunk/sysroots/x86_64-pokysdk-linux
OECORE_TARGET_OS=linux-gnueabi
OECORE_TARGET_ARCH=arm
OECORE_TARGET_SYSROOT=/home/igalia/psaavedra/toolchain_env/wandboard-mesa-wpe-trunk/sysroots/armv7at2hf-neon-poky-linux-gnueabi
OE_CMAKE_TOOLCHAIN_FILE=/home/igalia/psaavedra/toolchain_env/wandboard-mesa-wpe-trunk/sysroots/x86_64-pokysdk-linux/usr/share/cmake/OEToolchainConfig.cmake
OE_CMAKE_FIND_LIBRARY_CUSTOM_LIB_SUFFIX=
OECORE_BASELIB=lib

$ env | grep poky
...
CC=arm-poky-linux-gnueabi-gcc  -march=armv7-a -mthumb -mfpu=neon-mfloat-abi=hard --sysroot=/home/igalia/psaavedra/toolchain_env/wandboard-mesa-wpe-trunk/sysroots/armv7at2hf-neon-poky-linux-gnueabi
CXX=arm-poky-linux-gnueabi-g++  -march=armv7-a -mthumb -mfpu=neon -mfloat-abi=hard --sysroot=/home/igalia/psaavedra/toolchain_env/wandboard-mesa-wpe-trunk/sysroots/armv7at2hf-neon-poky-linux-gnueabi
...
```


# Cross-compiling WPE

## Copy the sources

```
wget -O WebKit-SVN-source.tar.bz2 https://s3-us-west-2.amazonaws.com/archives.webkit.org/WebKit-SVN-source.tar.bz2
cd ${WORKDIR}
tar -xf WebKit-SVN-source.tar.bz2
```

```
cd ${WORKDIR}/webkit
rm -rf WebKitBuild/Release/
./Tools/Scripts/build-webkit --release --wpe --cmakeargs="-DBUILD_SHARED_LIBS=ON" --no-experimental-features --no-video --no-web-rtc
```

## Build product archive WPE

```
cd ${WORKDIR}/webkit
python ./Tools/BuildSlaveSupport/built-product-archive --platform=wpe --release
archive
# release.zip
```

## Extract product archive webkit

In a remote Wandboard flashed with `core-image-wpe-base`.

```
cd ${WORKDIR}/webkit
python ./Tools/BuildSlaveSupport/built-product-archive --platform=wpe --release
extract
```

# Executing Layout tests


```
export NUMBER_OF_PROCESSORS=2
export WEBKIT_TEST_CHILD_PROCESSES=2
export WAYLAND_DISPLAY=wayland-0
export XDG_RUNTIME_DIR=/run/user/`id -u`
```

```
cd ${WORKDIR}/webkit
mkdir -p  WebKitBuild/DependenciesWPE/Root/
pushd  WebKitBuild/DependenciesWPE/Root/
git clone https://github.com/WebKitGTK/webkitgtk-test-fonts.git
git clone https://github.com/mrobinson/webkitgtk-test-dicts.git
```

```
perl ./Tools/Scripts/run-javascriptcore-tests --no-build --no-fail-fast
--json-output=jsc_results.json --release --memory-limited --wpe
```

```
python ./Tools/Scripts/run-webkit-tests --no-show-results --no-new-test-results
--no-sample-on-timeout --results-directory layout-test-results
--debug-rwt-logging --release --wpe --no-retry-failures --no-http-servers
--no-build --time-out-ms=60000 --exit-after-n-crashes-or-timeouts=3
--skipped=always  animations/import.html
http/tests/workers/service/controller-change.html
```

```
python ./Tools/Scripts/run-webkit-tests --no-show-results --no-new-test-results
--no-sample-on-timeout --results-directory prueba-layout-test-results --release
--wpe --no-http-servers --no-build --time-out-ms=20000
--exit-after-n-crashes-or-timeouts=50 --skipped=always
```
